 /.# Mathymbols (Math symbols)

2022.09.14 初步想法 v1.0  
2022.09.17 Pushed by VSCode
2022.09.17 Pulled from GitLib

$$
a+b
$$

⼀、常⽤数学符号markdown表⽰  
| 第一列 | 第二列 |
| aa | bb | cc |
| --- | --- | --- |
| dd | ee | ff |
<br>

表  
| 默认格式  | 左对齐 | 居中 | 右对齐 |
| ------ | :------  | :------: | ------: |
| 默认格式表格内容  | 左对齐表格内容 | 居中表格内容 | 右对齐表格内容 |
| 默认格式表格内容  | 左对齐表格内容 | 居中表格内容 | 右对齐表格内容 |

<br>

| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |


乘号，正负号: $\times$ $\pm$  

除号，竖线: $\div$ $\mid$  

⋅点：: $\cdot$  

∘圆: $\circ$  
⨂克罗内克积: $\bigotimes$  
⨁异或: $\bigoplus$  
≤≥=⼩于等于，⼤于等于 不等于: $\leq$ $\geq$ $\neq$  
≈约等于: $\approx$  
∫∬∮积分，双重积分，曲线积分:$\int$ $\iint$ $\oint$  
∞⽆穷: $\infty$  
∇梯度: $\nabla$  
∵∴因为，所以 和 $\because$ 和 $\therefore$  
∀
∃
任意和存在:  和 $\forall$ 和 $\exists$
/
∈
∈
属于和不属于:  和 $\in$ 和 $\notin$
⊂
⊆
∅
⼦集，真⼦集，空集
: 
，
，
$\subset$，$\subseteq$，$\emptyset$
⋂
⋃
交集和并集
:
 和 
$\bigcap$ 和 $\bigcup$
⋁
⋀
逻辑或 和 逻辑与:
 和 
$\bigvee$ 和 $\bigwedge$
^
y
期望值: $\hat{y}$
a+b+c+d&#10;
